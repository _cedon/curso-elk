###################################################################
# Chapter 1: Elastic Stack Overview


###################################################################
# Chapter 2: Getting Started with Elasticsearch


###################################################################
# Chapter 3: CRUD Operations

DELETE my_index
DELETE comments
DELETE .kibana
DELETE weather

#Getting Data In
PUT my_blogs/_doc/1
{
  "title": "Solving the Small but Important Issues with Fix-It Fridays",
  "category": "Culture",
  "date": "December 22, 2017",
  "author": {
    "first_name": "Daniel",
    "last_name": "Cecil",
    "company": "Elastic"
  }
}

POST /my_blogs/_doc/
{
  "title": "Fighting Ebola with Elastic",
  "category": "User Stories",
  "author": {
    "first_name": "Emily",
    "last_name": "Mosher"
  }
}

PUT my_blogs/_doc/1
{
  "title": "Elasticsearch 5.0.0-beta1 released",
  "category": "Releases",
  "date": "September 22, 2016",
  "author": {
    "first_name": "Clinton",
    "last_name": "Gormley",
    "company": "Elastic"
  }
}

PUT my_blogs/_doc/1/_create
{
  "title": "Elasticsearch 5.0.0-beta1 released",
  "category": "Releases",
  "date": "September 22, 2016",
  "author": {
    "first_name": "Clinton",
    "last_name": "Gormley",
    "company": "Elastic"
  }
}

POST my_blogs/_doc/1/_update
{
  "doc": {
    "date": "September 26, 2016"
  }
}

DELETE my_blogs/_doc/1

#Getting Data Out
GET my_blogs/_doc/1

GET my_blogs/_search

GET my_blogs/_search
{
  "query": {
    "match_all": {}
  }
}

GET my_blogs/_search?q=*

GET my_index/_search
{
  "query": {
    "match": {
      "title": "elasticsearch"
    }
  }
}

GET my_blogs/_search?q=title:elasticsearch


###################################################################
#Chapter 4: Querying Data
#Searching for Terms
GET blogs/_search
{
  "_source": "title",
  "query": {
    "match": {
      "content": "ingest nodes"
    }
  }
}

GET blogs/_search
{
  "query": {
    "match": {
      "content": "ingest nodes"
    }
  }
}

GET blogs/_search?q=content:(ingest nodes)

GET blogs/_search
{
  "query": {
    "match": {
      "content": {
        "query": "ingest nodes",
        "operator": "and"
      }
    }
  }
}

GET blogs/_search?q=content:(ingest AND nodes)

GET blogs/_search
{
  "query": {
    "match": {
      "content": "ingest nodes logstash"
    }
  }
}

GET blogs/_search?q=content:(ingest nodes logstash)

GET blogs/_search
{
  "query": {
    "match": {
      "content": {
        "query": "ingest nodes logstash",
        "minimum_should_match": 2
      }
    }
  }
}

#Searching for Phrases
GET blogs/_search
{
  "query": {
    "match_phrase": {
      "content": "ingest nodes"
    }
  }
}

GET blogs/_search?q=content:("ingest nodes")

GET blogs/_search
{
  "query": {
    "match_phrase": {
      "content": "open data"
    }
  }
}

GET blogs/_search?q=content:"open data"

GET blogs/_search
{
  "query": {
    "match_phrase": {
      "content": {
        "query": "open data",
        "slop": 1
      }
    }
  }
}

#Searching within Date Ranges
GET blogs/_search
{
  "query": {
    "range": {
      "publish_date": {
        "gte": "2017-12-01",
        "lt": "2018-01-01"
      }
    }
  }
}

GET blogs/_search
{
  "query": {
    "bool": {
      "must": {"match": {"content": "security"}},
      "filter": {
        "range": {
          "publish_date": {
            "gte": "2017-12-01",
            "lt": "2018-01-01"
          }
        }
      }
    }
  }
}

GET blogs/_search
{
  "query": {
    "bool": {
      "must": {"match": {"content": "security"}},
      "filter": {
        "range": {
          "publish_date": {
            "gte": "now-3M"
          }
        }
      }
    }
  }
}

#Combining Searches
GET blogs/_search
{
  "query": {
    "bool": {
      "must": [
        {
          "match": {
            "content": "logstash"
          }
        },
        {
          "match": {
            "category": "engineering"
          }
        }
      ]
    }
  }
}

GET blogs/_search
{
  "query": {
    "bool": {
      "must": {
          "match": {
            "content": "logstash"
          }
        },
      "must_not": {
          "match": {
            "category": "releases"
          }
        }
    }
  }
}

GET blogs/_search
{
  "query": {
    "match_phrase": {
      "content": "elastic stack"
    }
  }
}

GET blogs/_search
{
  "query": {
    "bool": {
      "must": {
        "match_phrase": {
          "content": "elastic stack"
        }
      },
      "should": {
        "match_phrase": {
          "author": "shay banon"
        }
      }
    }
  }
}

#Filtering Searches
GET blogs/_search
{
  "query": {
    "bool": {
      "must": {
        "match_phrase": {
          "content": "elastic stack"
        }
      },
      "filter": {
        "range": {
          "publish_date": {
            "lt": "2017-01-01"
          }
        }
      }
    }
  }
}

#Controlling Precision
GET blogs/_search
{
  "query": {
    "bool": {
      "must": [
        {"match": {"title": "elastic"}}
      ],
      "should": [
        {"match": {"title": "stack"}},
        {"match": {"title": "query"}},
        {"match": {"title": "speed"}}
      ]
    }
  }
}

GET blogs/_search
{
  "query": {
    "bool": {
      "must": [
        {"match": {"title": "elastic"}}
      ],
      "should": [
        {"match": {"title": "stack"}},
        {"match": {"title": "query"}},
        {"match": {"title": "speed"}}
      ],
      "minimum_should_match": 1
    }
  }
}

GET blogs/_search
{
  "query": {
    "bool": {
      "should": [
        {"match": {"title": "stack"}},
        {"match": {"title": "query"}},
        {"match": {"title": "speed"}}
      ]
    }
  }
}

GET blogs/_search
{
  "query": {
    "bool": {
      "must": {"match": {"title": "elastic"}},
      "should": {
        "bool": {
          "must_not": {
            "match": {
              "title": "stack"
            }
          }
        }
      }
    }
  }
}

GET blogs/_search
{
  "query": {
    "match": {
      "content": "open data"
    }
  }
}

GET blogs/_search
{
  "query": {
    "bool": {
      "must": {
        "match": {"content": "open data"}
      },
      "should": {
        "match_phrase": {"content": "open data"}
      }
    }
  }
}


###################################################################
# Chapter 5: Text Analysis and Mappings
#What is a Mapping?
GET my_index/_mapping

GET blogs/_mapping

GET blogs/_search?size=100

#Analyzers
POST _analyze
{
  "text": ["We are excited to introduce the world to X-Pack."],
  "analyzer": "standard"
}


###################################################################
# Chapter 6: Custom Mappings
#Defining a Custom Analyzer
DELETE blogs_test

PUT blogs_test
{
  "mappings": {
    "_doc": {
      "properties": {
        "locales": {
          "type": "keyword"
        }
      }
    }
  }
}

DELETE blogs_test

PUT blogs_test
{
  "settings": {
    "analysis": {
      "char_filter": {
        "xpack_filter": {
          "type": "mapping",
          "mappings": ["X-Pack => XPack"]
        }
      },
      "analyzer": {
        "my_content_analyzer": {
          "type": "custom",
          "char_filter": ["xpack_filter"],
          "tokenizer": "standard",
          "filter": ["lowercase"]
        }
      }
    }
  },
  "mappings": {
    "_doc": {
      "properties": {
        "content": {
          "type": "text",
          "analyzer": "my_content_analyzer"
        }
      }
    }
  }
}

POST blogs_test/_analyze
{
  "text": ["We love X-Pack"],
  "analyzer": "my_content_analyzer"
}

DELETE blogs_test2

PUT blogs_test2
{
  "settings": {
    "analysis": {
      "char_filter": {
        "xpack_filter": {
          "type": "mapping",
          "mappings": ["X-Pack => XPack"]
        }
      },
      "filter": {
        "my_snowball_filter": {
          "type": "snowball",
          "language": "English"
        }
      },
      "analyzer": {
        "my_content_analyzer": {
          "type": "custom",
          "char_filter": ["xpack_filter"],
          "tokenizer": "standard",
          "filter": ["lowercase","stop","my_snowball_filter"]
        }
      }
    }
  },
  "mappings": {
    "_doc": {
      "properties": {
        "content": {
          "type": "text",
          "analyzer": "my_content_analyzer"
        }
      }
    }
  }
}

POST blogs_test2/_analyze
{
  "text": ["Autodiscovery allows the user to configure providers"],
  "analyzer": "my_content_analyzer"
}

#Improving the Blog Mapping
GET blogs/_search?size=100

GET blogs/_search
{
  "query": {
    "match": {
      "author": "kim"
    }
  }
}

GET blogs/_search
{
  "query": {
    "bool": {
      "filter": {
        "match": {
          "author": "Bohyun Kim"
        }
      }
    }
  }
}


#Define Explicit Mappings
PUT blogs_temp/_doc/1
{
  "date": "December 22, 2017",
  "author": "Firstname Lastname",
  "title": "Elastic Advent Calendar 2017, Week 3",
  "seo_title": "A Good SEO Title",
  "url": "/blog/some-url",
  "content": "blog content",
  "locales": "ja-jp",
  "@timestamp": "2017-12-22T07:00:00.000Z",
  "category": "Engineering"
}

GET blogs_temp/_mapping

PUT blogs_orig

POST _reindex
{
  "source": {
    "index": "blogs"
  },
  "dest": {
    "index": "blogs_orig"
  }
}

DELETE blogs

PUT blogs
{
  "mappings": {
    "_doc": {
      "properties": {
        "@timestamp": {
          "type": "date"
        },
        "author": {
          "type": "text",
          "fields": {
            "keyword": {
              "type": "keyword",
              "ignore_above": 256
            }
          }
        },
        "category": {
          "type": "keyword"
        },
        "content": {
          "type": "text"
        },
        "date": {
          "type": "date",
          "format": [
            "M dd, yyyy"
          ]
        },
        "locales": {
          "type": "text"
        },
        "seo_title": {
          "type": "text",
          "fields": {
            "keyword": {
              "type": "keyword",
              "ignore_above": 256
            }
          }
        },
        "title": {
          "type": "text",
          "fields": {
            "keyword": {
              "type": "keyword",
              "ignore_above": 256
            }
          }
        },
        "url": {
          "type": "keyword"
        }
      }
    }
  }
}


###################################################################
#Chapter 7: Node Types

GET _cluster/state

GET _cat/nodes


###################################################################
#Chapter 8: Understanding Shards

PUT my_new_index
{
  "settings": {
    "number_of_shards": 3,
    "number_of_replicas": 2
  }
}

GET _cat/shards

GET blogs/_search?explain=true
{
  "query": {
    "match": {
      "author": "Cholakian"
    }
  }
}

GET blogs/_doc/25


#############################################################
# Chapter 9: Troubleshooting Elasticsearch
#Configuration Settings
PUT my_tweets
{
  "settings": {
    "number_of_shards": 3
  }
}

PUT my_tweets/_settings
{
  "index.blocks.write": false
}

PUT _cluster/settings
{
  "persistent": {
    "discovery.zen.minimum_master_nodes": 2
  }
}

PUT _cluster/settings
{
  "transient" : {
        "logger.org.elasticsearch.discovery" : "DEBUG"
  }
}

#Anatomy of a Search
GET blogs/_search?search_type=dfs_query_then_fetch

#Elasticsearch Responses
DELETE test

PUT test/_doc/1
{
  "test":"test"
}

#Cluster Health
GET _cluster/health

PUT my_index_1
{
  "settings": {
    "number_of_shards": 1,
    "number_of_replicas": 2
  }
}

PUT my_index_2
{
  "settings": {
    "number_of_shards": 1,
    "number_of_replicas": 3
  }
}

PUT my_index_3
{
  "settings": {
    "number_of_shards": 1,
    "number_of_replicas": 1
  }
}

#Diagnosing Health Issues
GET _cluster/health

GET _cluster/health?level=indices

GET _cluster/health/my_index

GET _cluster/health?level=shards

GET _cluster/allocation/explain

GET _cluster/allocation/explain
{
  "index" : "my_index",
  "shard" : 0,
  "primary" : true
}


#############################################################
# Chapter 10: Improving Search Results

#Multi-field Searches
GET blogs/_search
{
  "query": {
    "multi_match": {
      "query": "shay banon",
      "fields": [
        "title",
        "content",
        "author"
      ],
      "type": "best_fields"
    }
  }
}

GET blogs/_search
{
  "query": {
    "match": {
      "title": "shay banon"
    }
  }
}

GET blogs/_search
{
  "query": {
    "match": {
      "author": "shay banon"
    }
  }
}

GET blogs/_search
{
  "query": {
    "match": {
      "content": "shay banon"
    }
  }
}


GET blogs/_search
{
  "query": {
    "multi_match": {
      "query": "shay banon",
      "fields": [
        "title^2",
        "content",
        "author"
      ],
      "type": "best_fields"
    }
  }
}

GET blogs/_search
{
  "query": {
    "multi_match": {
      "query": "elasticsearch training",
      "fields": [
        "title^2",
        "content",
        "author"
      ],
      "type": "best_fields"
    }
  }
}

GET blogs/_search
{
  "query": {
    "multi_match": {
      "query": "elasticsearch training",
      "fields": [
        "title^2",
        "content",
        "author"
      ],
      "type": "phrase"
    }
  }
}

#Mispelt Werds
GET blogs/_search
{
  "query": {
    "match": {
      "content": "shard"
    }
  }
}

GET blogs/_search
{
  "query": {
    "match": {
      "content": "shark"
    }
  }
}

GET blogs/_search
{
  "query": {
    "match": {
      "content": {
        "query": "shark",
        "fuzziness": 1
      }
    }
  }
}

GET blogs/_search
{
  "query": {
    "match": {
      "content": {
        "query": "monitering datu",
        "fuzziness": 1
      }
    }
  }
}

GET blogs/_search
{
  "size": 100,
  "_source": "author",
  "query": {
    "match": {
      "author": {
        "query": "hi world",
        "fuzziness": 2
      }
    }
  }
}

GET blogs/_search
{
  "size": 100,
  "_source": "author",
  "query": {
    "match": {
      "author": {
        "query": "hi world",
        "fuzziness": "auto"
      }
    }
  }
}

#Searching for Exact Terms
GET blogs/_search
{
  "_source": "author",
  "query": {
    "match": {
      "author": "Bohyun Kim"
    }
  }
}

GET blogs/_search
{
  "query": {
    "match": {
      "author.keyword": "Bohyun Kim"
    }
  }
}

GET blogs/_search
{
  "query": {
    "bool": {
      "must": {
        "match": {
          "content": "monitoring"
        }
      },
      "filter": {
        "match": {
          "author.keyword": "Bohyun Kim"
        }
      }
    }
  }
}

#Sorting
GET blogs/_search
{
  "query": {
    "match": {
      "content": "security"
    }
  }
}

GET blogs/_search
{
  "query": {
    "match": {
      "content": "security"
    }
  },
  "sort": [
    {
      "publish_date": {
        "order": "desc"
      }
    }
  ]
}

GET blogs/_search
{
  "query": {
    "bool": {
      "must": {"match": {"content": "security"}},
      "must_not": {"match": {"author.keyword": ""}}
    }
  },
  "sort": [
    {
      "author.keyword": {
        "order": "asc"
      }
    },
    {
      "publish_date": {
        "order": "desc"
      }
    }
  ]
}

GET blogs/_search
{
  "query": {
    "bool": {
      "must": {"match": {"content": "security"}},
      "must_not": {"match": {"author.keyword": ""}}
    }
  },
  "sort": [
    {"author.keyword": {"order": "asc"}},
    {"publish_date": {"order": "desc"}},
    {"_id": {"order": "asc"}}
  ]
}


#Paging

GET blogs/_search
{
  "size": 10,
  "query": {
    "match": {
      "content": "elasticsearch"
    }
  }
}

GET blogs/_search
{
  "from": 10,
  "size": 10,
  "query": {
    "match": {
      "content": "elasticsearch"
    }
  }
}

#Avoid deep pagination!
GET blogs/_search
{
  "from": 990,
  "size": 10,
  "query": {
    "match": {
      "content": "elasticsearch"
    }
  }
}

#Use search_after instead:
GET blogs/_search
{
  "size": 10,
  "query": {
    "match": {
      "content": "elasticsearch"
    }
  },
  "sort": [
    {"_score": {"order": "desc"}},
    {"_id": {"order":"asc"}}
  ]
}

GET blogs/_search
{
  "size": 10,
  "query": {
    "match": {
      "content": "elasticsearch"
    }
  },
  "sort": [
    {"_score": {"order": "desc"}},
    {"_id": {"order":"asc"}}
  ],
  "search_after": [
    0.55449957,
    "1346"
  ]
}

#Highlighting
GET blogs/_search
{
  "query": {
    "match_phrase": {
      "title": "kibana"
    }
  },
  "highlight": {
    "fields": {
      "title": {}
    }
  }
}

GET blogs/_search
{
  "query": {
    "match_phrase": {
      "title": "kibana"
    }
  },
  "highlight": {
    "fields": {
      "title": {},
      "content": {}
    }
  }
}

GET blogs/_search
{
  "query": {
    "match_phrase": {
      "title": "kibana"
    }
  },
  "highlight": {
    "fields": {
      "title": {},
      "content": {}
    },
    "require_field_match": false
  }
}

GET blogs/_search
{
  "query": {
    "match_phrase": {
      "title": "kibana"
    }
  },
  "highlight": {
    "fields": {
      "title": {}
    },
    "pre_tags": ["<es-hit>"],
    "post_tags": ["</es-hit>"]
  }
}


#############################################################
# Chapter 11: Aggregating Data
GET _cat/indices

GET logs_server*/_search
{
  "aggs": {
    "average_response_size": {
      "avg": {
        "field": "response_size"
      }
    }
  }
}

GET logs_server*/_search
{
  "size": 0,
  "aggs": {
    "average_response_size": {
      "avg": {
        "field": "response_size"
      }
    }
  }
}

GET logs_server*/_search
{
  "size": 0,
  "query": {
    "match": {
      "language.code": "fr-fr"
    }
  },
  "aggs": {
    "average_french_response": {
      "avg": {
        "field": "response_size"
      }
    }
  }
}

#Answer some aggregation questions
GET logs_server*/_search
{
  "size": 0,
  "aggs": {
    "max_response_size": {
      "max": {
        "field": "response_size"
      }
    }
  }
}

GET logs_server*/_search
{
  "size": 0,
  "aggs": {
    "size_stats": {
      "stats": {
        "field": "response_size"
      }
    }
  }
}

GET logs_server1/_search

GET logs_server*/_search
{
  "size": 0,
  "aggs": {
    "number_of_countries": {
      "cardinality": {
        "field": "geoip.country_name.keyword"
      }
    }
  }
}


GET logs_server*/_search
{
  "size": 0,
  "aggs": {
    "logs_by_month": {
      "date_histogram": {
        "field": "@timestamp",
        "interval": "month"
      }
    }
  }
}

GET logs_server*/_search
{
  "size": 0,
  "aggs": {
    "logs_by_month": {
      "date_histogram": {
        "field": "@timestamp",
        "interval": "month"
      },
      "aggs": {
        "average_size": {
          "avg": {
            "field": "response_size"
          }
        }
      }
    }
  }
}

GET logs_server1/_search


#The terms Aggregation
GET logs_server*/_search
{
  "size": 0,
  "aggs": {
    "country_name_terms": {
      "terms": {
        "field": "geoip.country_name.keyword",
        "size": 5
      }
    }
  }
}


GET logs_server*/_search
{
  "size": 0,
  "aggs": {
    "top_10_urls": {
      "terms": {
        "field": "originalUrl.keyword",
        "size": 10
      }
    }
  }
}

GET logs_server*/_search
{
  "size": 0,
  "aggs": {
    "top_10_urls": {
      "terms": {
        "field": "originalUrl.keyword",
        "size": 10,
        "shard_size": 500
      }
    }
  }
}

GET logs_server*/_search
{
  "size": 0,
  "aggs": {
    "top_10_urls": {
      "terms": {
        "field": "originalUrl.keyword",
        "size": 10,
        "show_term_doc_count_error": true
      }
    }
  }
}

GET logs_server*/_search
{
  "size": 0,
  "aggs": {
    "top_10_urls": {
      "terms": {
        "field": "originalUrl.keyword",
        "size": 10,
        "show_term_doc_count_error": true,
        "shard_size": 10
      }
    }
  }
}

DELETE countries
PUT countries
{
  "settings": {
    "number_of_shards": 2,
    "number_of_replicas": 0
  },
  "mappings": {
    "_doc": {
      "properties": {
        "country": {
          "type": "keyword"
        }
      }
    }
  }
}

PUT countries/_doc/_bulk
{"index" : {"routing":"a"}}
{"country": "USA"}
{"index" : {"routing":"a"}}
{"country": "USA"}
{"index" : {"routing":"a"}}
{"country": "USA"}
{"index" : {"routing":"a"}}
{"country": "USA"}
{"index" : {"routing":"a"}}
{"country": "USA"}
{"index" : {"routing":"b"}}
{"country": "USA"}
{"index" : {"routing":"b"}}
{"country": "USA"}
{"index" : {"routing":"b"}}
{"country": "USA"}
{"index" : {"routing":"b"}}
{"country": "USA"}
{"index" : {"routing":"a"}}
{"country": "India"}
{"index" : {"routing":"a"}}
{"country": "India"}
{"index" : {"routing":"a"}}
{"country": "India"}
{"index" : {"routing":"a"}}
{"country": "India"}
{"index" : {"routing":"b"}}
{"country": "India"}
{"index" : {"routing":"b"}}
{"country": "India"}
{"index" : {"routing":"b"}}
{"country": "India"}
{"index" : {"routing":"b"}}
{"country": "India"}
{"index" : {"routing":"b"}}
{"country": "India"}
{"index" : {"routing":"a"}}
{"country": "Japan"}
{"index" : {"routing":"a"}}
{"country": "Japan"}
{"index" : {"routing":"a"}}
{"country": "Japan"}
{"index" : {"routing":"b"}}
{"country": "Japan"}
{"index" : {"routing":"b"}}
{"country": "Japan"}
{"index" : {"routing":"b"}}
{"country": "Japan"}
{"index" : {"routing":"a"}}
{"country": "France"}
{"index" : {"routing":"a"}}
{"country": "France"}
{"index" : {"routing":"a"}}
{"country": "France"}
{"index" : {"routing":"a"}}
{"country": "France"}
{"index" : {"routing":"b"}}
{"country": "France"}

GET _cat/shards/countries?v

GET countries/_search
{
  "size": 0,
  "aggs": {
    "NAME": {
      "terms": {
        "field": "country",
        "size": 3,
        "shard_size": 3,
        "show_term_doc_count_error": true
      }
    }
  }
}

GET countries/_search
{
  "size": 0,
  "aggs": {
    "NAME": {
      "terms": {
        "field": "country",
        "size": 3,
        "shard_size": 5,
        "show_term_doc_count_error": true
      }
    }
  }
}

GET logs_server*/_search
{
  "size": 0,
  "aggs": {
    "top_10_urls": {
      "terms": {
        "field": "originalUrl.keyword",
        "size": 10,
        "show_term_doc_count_error": true,
        "shard_size": 20
      }
    }
  }
}


#More aggregation questions
GET logs_server*/_search
{
  "size": 0,
  "aggs": {
    "logs_by_month": {
      "date_histogram": {
        "field": "@timestamp",
        "interval": "month"
      },
      "aggs": {
        "country_name": {
          "terms": {
            "field": "geoip.country_name.keyword",
            "size": 10
          }
        }
      }
    }
  }
}

GET logs_server*/_search
{
  "size": 0,
  "aggs": {
    "logs_by_month": {
      "date_histogram": {
        "field": "@timestamp",
        "interval": "month",
        "order": {
          "_key": "desc"
        }
      }
    }
  }
}

GET logs_server*/_search
{
  "size": 0,
  "aggs": {
    "logs_by_month": {
      "date_histogram": {
        "field": "@timestamp",
        "interval": "month",
        "order": {
          "average_runtime": "asc"
        }
      },
      "aggs": {
        "average_runtime": {
          "avg": {
            "field": "runtime_ms"
          }
        }
      }
    }
  }
}

GET logs_server*/_search
{
  "size": 0,
  "aggs": {
    "missing_latitude": {
      "missing": {
        "field": "geoip.location.lat"
      }
    },
    "missing_longitude": {
      "missing": {
        "field": "geoip.location.lon"
      }
    }
  }
}


########################################################
# Chapter 12: Best Practices

#Index Aliases
POST _aliases
{
  "actions": [
    {
      "add": {
        "index": "blogs_v1",
        "alias": "blogs"
      }
    }
  ]
}

POST _aliases
{
  "actions": [
    {
      "add": {
        "index": "blogs_v2",
        "alias": "blogs"
      }
    },
    {
      "remove": {
        "index": "blogs_v1",
        "alias": "blogs"
      }
    }
  ]
}

POST _aliases
{
  "actions": [
    {
      "add": {
        "index": "blogs_v1",
        "alias": "blogs_engineering",
        "filter": {
          "match": {
            "category": "Engineering"
          }
        }
      }
    }
  ]
}

#Index Templates
GET logs_server1/_mapping

PUT _template/logs_template
{
  "index_patterns": "logs-*",
  "order": 1,
  "settings": {
    "number_of_shards": 4,
    "number_of_replicas": 1
  },
  "mappings": {
    "doc": {
      "properties": {
        "@timestamp": {
          "type": "date"
        }
      }
    }
  }
}

PUT logs-2017-12

GET logs-2017-12

PUT _template/logs_2018_template
{
  "index_patterns": "logs-2018*",
  "order": 5,
  "settings": {
    "number_of_shards": 6,
    "number_of_replicas": 2
  }
}

PUT logs-2018-06
GET logs-2018-06

#Cheaper in Bulk
POST comments/_doc/_bulk
{"index" : {"_id":3}}
{"username": "ricardo", "movie": "Star Wars: Rogue One","comment": "Brilliant!", "rating": 5}
{"index" : {"_id":4}}
{"username": "paolo", "movie": "Star Trek IV: The Voyage Home","comment": "Not my favorite  :(", "rating": 2}
{"index" : {"_id":5}}
{"username": "harrison", "movie": "Blade Runner","comment": "A classic movie", "rating": 4}
{"update" : {"_id":5}}
{"doc": {"rating": 5}}
{"delete": {"_id":4}}

GET /_mget
{
  "docs" : [
    {
      "_index" : "comments",
      "_type" : "_doc",
      "_id" : 3
    },
    {
      "_index" : "blogs",
      "_type" : "_doc",
      "_id" : "F1oSq2EBOOytT5ZTHpaE"
    }
    ]
}

#Scroll Searches
GET logs-2018-03/_search?scroll=30s
{
  "size": 1000,
  "query": {
    "match_all": {}
  },
  "sort": [
    {
      "_doc": {
        "order": "asc"
      }
    }
  ]
}

GET _search/scroll
{
  "scroll": "30s",
  "scroll_id": "DnF1ZXJ5VGhlbkZldGNoBQAAAAAAAAWOFnlWQVR3N3pxUjdLMn-JLcUZpSDVkWWcAAAAAAAAFjAAAAWPFnlWQVR3N3pxUjdLMnJLcUZpSDVkWWcAAAAAAAAFkRZ5VkFUdzd6cVI3SzJyS3FGaUg1ZFln"
}

DELETE _search/scroll/DnF1ZXJ5VGhlbkZldGNoBQAAAAAAAAWOFnlWQVR3N3pxUjdLMn-JLcUZpSDVkWWcAAAAAAAAFjAAAAWPFnlWQVR3N3pxUjdLMnJLcUZpSDVkWWcAAAAAAAAFkRZ5VkFUdzd6cVI3SzJyS3FGaUg1ZFln

#Cluster Backup
PUT _snapshot/my_repo
{
  "type": "fs",
  "settings": {
    "location": "/mnt/my_repo_folder"
  }
}

PUT _snapshot/my_repo
{
  "type": "fs",
  "settings": {
    "location": "/mnt/my_repo_folder",
    "compress": true,
    "max_restore_bytes_per_sec": "40mb",
    "max_snapshot_bytes_per_sec": "40mb"
  }
}

PUT _snapshot/my_s3_repo
{
  "type": "s3",
  "settings": {
    "bucket": "my_s3_bucket_name"
  }
}

#Taking a Snapshot
PUT _snapshot/my_repo/my_snapshot_1

PUT _snapshot/my_repo/my_logs_snapshot_1
{
  "indices": "logs-*",
  "ignore_unavailable": true,
  "include_global_state": true
}

GET _snapshot/my_repo/my_snapshot_2/_status

GET _snapshot/my_repo/_all

GET _snapshot/my_repo/my_snapshot_1

DELETE _snapshot/my_repo/my_snapshot_1

#Restoring From a Snapshot
POST _snapshot/my_repo/my_snapshot_2/_restore

POST _snapshot/my_repo/my_snapshot_2/_restore
{
  "indices": "logs-*",
  "ignore_unavailable": true,
  "include_global_state": false,
  "rename_pattern": "logs-(.+)",
  "rename_replacement": "restored-logs-$1"
}

