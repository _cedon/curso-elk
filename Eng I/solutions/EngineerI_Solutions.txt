########################################################
# Lab 1: Elastic Stack Overview

######

input {
  jdbc {
    jdbc_connection_string => "jdbc:postgresql://db_server:5432/"
    jdbc_driver_class => "org.postgresql.Driver"
    jdbc_driver_library => "/home/elastic/postgresql-9.1-901-1.jdbc4.jar"
    jdbc_user => "postgres"
    jdbc_password => "password"
    statement => "SELECT * from blogs"
  }
}

filter {
  mutate {
    remove_field => ["@version", "host", "message", "@timestamp", "id", "tags"]
  }
}

output {
  stdout { codec => "dots"}
  elasticsearch {
    index => "blogs"
  }
}

######

{
  "count" : 1594,
  "_shards" : {
    "total" : 5,
    "successful" : 5,
    "skipped" : 0,
    "failed" : 0
  }
}

########################################################
# Lab 2: Getting Started with Elasticsearch

######

GET _search

######

GET /

######

{
  "name" : "Xe6KFUY",
  "cluster_name" : "elasticsearch",
  "cluster_uuid" : "tabF6ZRLTLq7pF9B1brrIg",
  "version" : {
    "number" : "6.6.1",
    "build_flavor" : "default",
    "build_type" : "tar",
    "build_hash" : "8c58350",
    "build_date" : "2018-11-16T02:22:42.182257Z",
    "build_snapshot" : false,
    "lucene_version" : "7.5.0",
    "minimum_wire_compatibility_version" : "5.6.0",
    "minimum_index_compatibility_version" : "5.0.0"
  },
  "tagline" : "You Know, for Search"
}

######

GET /_cat/indices?v

######

health status index        uuid                   pri rep docs.count docs.deleted store.size pri.store.size
yellow open   logs_server3 NaNptoaZS0-JFSuic-iM2w   5   1     584814            0    205.9mb        205.9mb
yellow open   blogs        AvXr9GhyRDmf0YFt0deb9w   5   1       1594            0      6.1mb          6.1mb
green  open   .kibana_1    hRbJq6fHQYqwvtPKoMSlFA   1   0          7            0     38.6kb         38.6kb
yellow open   logs_server1 G7GSLaWyTEac4y07DLgw-w   5   1     582063            0    202.6mb        202.6mb
yellow open   logs_server2 dU_NG_94RgqCq_TgIEEQLA   5   1     584599            0    206.1mb        206.1mb

######

# Use a descriptive name for your cluster:
#
cluster.name: my_cluster

######

GET /
GET /_cat/indices?v

######

# Xms represents the initial size of total heap space
# Xmx represents the maximum size of total heap space

-Xms512m
-Xmx512m

######

POST _xpack/license/start_trial?acknowledge=true

######

xpack.security.enabled: true

######

elasticsearch.username: "kibana"
elasticsearch.password: "password"

########################################################
# Lab 3: CRUD Operations

######

{
  "id": "1",
  "title": "Better query execution",
  "category": "Engineering",
  "date":"July 15, 2015",
  "author":{
    "first_name": "Adrian",
    "last_name": "Grand",
    "company": "Elastic"
  }
}

######

{
  "id": "2",
  "title": "The Story of Sense",
  "date":"May 28, 2015",
  "author":{
    "first_name": "Boaz",
    "last_name": "Leskes"
  }
}

######

PUT my_blogs/_doc/1
{
  "id": "1",
  "title": "Better query execution",
  "category": "Engineering",
  "date":"July 15, 2015",
  "author":{
    "first_name": "Adrian",
    "last_name": "Grand",
    "company": "Elastic"
  }
}
PUT my_blogs/_doc/2
{
  "id": "2",
  "title": "The Story of Sense",
  "date":"May 28, 2015",
  "author":{
    "first_name": "Boaz",
    "last_name": "Leskes"
  }
}

######

{
  "id": "57",
  "title": "Phrase Queries: a world without Stopwords",
  "date":"March 7, 2016",
  "category": "Engineering",
  "author":{
    "first_name": "Gabriel",
    "last_name": "Moskovicz"
  }
}

######

POST my_blogs/_doc/
{
  "id": "57",
  "title": "Phrase Queries: a world without Stopwords",
  "date":"March 7, 2016",
  "category": "Engineering",
  "author":{
    "first_name": "Gabriel",
    "last_name": "Moskovicz"
  }
}

######

GET my_blogs/_doc/1

######

{
  "_index": "my_blogs",
  "_type": "_doc",
  "_id": "2",
  "found": false
}

######

DELETE my_blogs/_doc/2

######

DELETE my_blogs

######

GET blogs*/_search

GET _cat/indices

PUT blogs/_doc/1
{
  "a": "b"
}

DELETE blogs

########################################################
# Lab 4: Querying Data

######

GET blogs/_search
{
  "query": {
    "match_all": {}
  }
}

######

GET blogs/_search
{
  "size": 100,
  "query": {
    "match_all": {}
  }
}

######

GET blogs/_search
{
  "size": 100,
  "_source": {
    "excludes": ["content"]
  },
  "query": {
    "match_all": {}
  }
}

######

GET blogs/_search
{
  "size": 100,
  "_source": {
    "includes": ["author","title"]
  },
  "query": {
    "match_all": {}
  }
}

######

GET blogs/_search
{
  "size": 100,
  "_source": ["author","title"],
  "query": {
    "match_all": {}
  }
}

######

GET blogs/_search
{
  "query": {
    "range": {
      "publish_date": {
        "gte": "2017-05-01",
        "lte": "2017-05-31"
      }
    }
  }
}

######

GET blogs/_search
{
  "query": {
    "match": {
      "title": "elastic"
    }
  }
}

######

GET blogs/_search
{
  "query": {
    "match": {
      "title": "elastic stack"
    }
  }
}

######

GET blogs/_search
{
  "query": {
    "match": {
      "title": {
        "query": "elastic stack",
        "operator": "and"
      }
    }
  }
}

######

GET blogs/_search
{
  "query": {
    "match": {
      "content": "search"
    }
  }
}

######

GET blogs/_search
{
  "query": {
    "match": {
      "content": "search analytics"
    }
  }
}

######

GET blogs/_search
{
  "query": {
    "match": {
      "content": {
        "query": "search analytics",
        "operator": "and"
      }
    }
  }
}

######

GET blogs/_search
{
  "size": 3,
  "query": {
    "match_phrase": {
      "content": "search analytics"
    }
  }
}

######

GET blogs/_search
{
  "query": {
    "match_phrase": {
      "content": {
        "query": "search analytics",
        "slop": 1
      }
    }
  }
}

######

GET blogs/_search
{
  "query": {
    "match": {
      "content": "performance optimizations improvements"
    }
  }
}

######

GET blogs/_search
{
  "query": {
    "match": {
      "content": {
        "query" : "performance optimizations improvements",
        "minimum_should_match" : 2
      }
    }
  }
}

######

GET blogs/_search
{
  "query": {
    "bool": {
      "must": [
        {
          "match": {
            "content": {
              "query": "performance optimizations improvements",
              "minimum_should_match": 2
            }
          }
        }
      ],
      "must_not": [
        {
          "match": {
            "title": "release releases released"
          }
        }
      ]
    }
  }
}

######

GET blogs/_search
{
  "query": {
    "bool": {
      "must": [
        {
          "match": {
            "content": {
              "query": "performance optimizations improvements",
              "minimum_should_match": 2
            }
          }
        }
      ],
      "must_not": [
        {
          "match": {
            "title": "release releases released"
          }
        }
      ],
      "should": [
        {
          "match": {
            "title": "elasticsearch"
          }
        }
      ]
    }
  }
}

########################################################
# Lab 5: Text Analysis and Mappings

######

{
  "title": "Elastic Cloud and Meltdown",
  "publish_date": "2018-01-07T23:00:00.000Z",
  "author": "Elastic Engineering",
  "category": "Engineering",
  "content": " Elastic is aware of the Meltdown and Spectre vulnerabilities...",
  "url": "/blog/elastic-cloud-and-meltdown",
  "locales": ["de-de","fr-fr"]
}

######

PUT tmp_blogs/_doc/1
{
  "title": "Elastic Cloud and Meltdown",
  "publish_date": "2018-01-07T23:00:00.000Z",
  "author": "Elastic Engineering",
  "category": "Engineering",
  "content": " Elastic is aware of the Meltdown and Spectre vulnerabilities...",
  "url": "/blog/elastic-cloud-and-meltdown",
  "locales": ["de-de","fr-fr"]
}

######

GET tmp_blogs/_mappings

######

{
  "tmp_blogs": {
    "mappings": {
      "_doc": {
        "properties": {
          "author": {
            "type": "text",
            "fields": {
              "keyword": {
                "type": "keyword",
                "ignore_above": 256
              }
            }
          },
          "category": {
            "type": "text",
            "fields": {
              "keyword": {
                "type": "keyword",
                "ignore_above": 256
              }
            }
          },
          "content": {
            "type": "text",
            "fields": {
              "keyword": {
                "type": "keyword",
                "ignore_above": 256
              }
            }
          },
          "locales": {
            "type": "text",
            "fields": {
              "keyword": {
                "type": "keyword",
                "ignore_above": 256
              }
            }
          },
          "publish_date": {
            "type": "date"
          },
          "title": {
            "type": "text",
            "fields": {
              "keyword": {
                "type": "keyword",
                "ignore_above": 256
              }
            }
          },
          "url": {
            "type": "text",
            "fields": {
              "keyword": {
                "type": "keyword",
                "ignore_above": 256
              }
            }
          }
        }
      }
    }
  }
}

######

DELETE tmp_blogs

PUT tmp_blogs
{
  "mappings": {
    "_doc": {
      "properties": {
        "publish_date": {
          "type": "date"
        },
        "author": {
          "type": "text",
          "fields": {
            "keyword": {
              "type": "keyword",
              "ignore_above": 256
            }
          }
        },
        "category": {
          "type": "keyword"
        },
        "content": {
          "type": "text"
        },
        "locales": {
          "type": "keyword"
        },
        "title": {
          "type": "text",
          "fields": {
            "keyword": {
              "type": "keyword",
              "ignore_above": 256
            }
          }
        },
        "url": {
          "type": "text",
          "fields": {
            "keyword": {
              "type": "keyword",
              "ignore_above": 256
            }
          }
        }
      }
    }
  }
}

######

PUT tmp_blogs/_doc/1
{
  "title": "Elastic Cloud and Meltdown",
  "publish_date": "2018-01-07T23:00:00.000Z",
  "author": "Elastic Engineering",
  "category": "Engineering",
  "content": " Elastic is aware of the Meltdown and Spectre vulnerabilities...",
  "url": "/blog/elastic-cloud-and-meltdown",
  "locales": ["de-de","fr-fr"]
}

######

GET tmp_blogs/_search
{
  "query": {
    "match": {
      "category": "engineering"
    }
  }
}

GET tmp_blogs/_search
{
  "query": {
    "match": {
      "category": "Engineering"
    }
  }
}

######

"Introducing beta releases: Elasticsearch and Kibana Docker images!"

######

GET _analyze
{
  "text": "Introducing beta releases: Elasticsearch and Kibana Docker images!"
}

######

GET _analyze
{
  "analyzer": "whitespace",
  "text": "Introducing beta releases: Elasticsearch and Kibana Docker images!"
}

######

GET _analyze
{
  "analyzer": "stop",
  "text": "Introducing beta releases: Elasticsearch and Kibana Docker images!"
}

GET _analyze
{
  "analyzer": "keyword",
  "text": "Introducing beta releases: Elasticsearch and Kibana Docker images!"
}

GET _analyze
{
  "analyzer": "english",
  "text": "Introducing beta releases: Elasticsearch and Kibana Docker images!"
}

######

"text": "This release includes mainly bug fixes."

######

GET _analyze
{
  "tokenizer": "standard",
  "filter": ["lowercase","snowball"],
  "text": "This release includes mainly bug fixes."
}

GET _analyze
{
  "analyzer": "english",
  "text": "This release includes mainly bug fixes."
}

######

"text": "Elasticsearch é um motor de buscas distribuído."

######

GET _analyze
{
  "tokenizer": "standard",
  "filter": ["lowercase", "asciifolding"],
  "text": "Elasticsearch é um motor de buscas distribuído."
}

######

"text": "C++ can help it and your IT systems."

######

GET _analyze
{
  "analyzer": "english",
  "text": "C++ can help it and your IT systems."
}

########################################################
# Lab 6: Custom Mappings

######

GET _analyze
{
  "analyzer": "english",
  "text": "C++ can help it and your IT systems."
}

PUT analysis_test
{
  "settings": {
    "analysis": {
      "char_filter": {
        "cpp_it": {
          "type": "mapping",
          "mappings": ["c++ => cpp", "C++ => cpp", "IT => _IT_"]
        }
      },
      "filter": {
        "my_stop": {
          "type": "stop",
          "stopwords": ["can", "we", "our", "you", "your", "all"]
        }
      },
      "analyzer": {
        "my_analyzer": {
          "tokenizer": "standard",
          "char_filter": ["cpp_it"],
          "filter": ["lowercase", "stop", "my_stop"]
        }
      }
    }
  }
}

GET analysis_test/_analyze
{
  "analyzer": "my_analyzer",
  "text": "C++ can help it and your IT systems."
}

######

GET blogs/_search
{
  "query": {
    "match": {
      "content": "c++"
    }
  }
}

GET blogs/_search
{
  "query": {
    "match": {
      "title": "IT"
    }
  }
}

######

PUT blogs_analyzed
{
  "settings": {
    "analysis": {
      "char_filter": {
        "cpp_it": {
          "type": "mapping",
          "mappings": [
            "c++ => cpp",
            "C++ => cpp",
            "IT => _IT_"
          ]
        }
      },
      "filter": {
        "my_stop": {
          "type": "stop",
          "stopwords": [
            "can",
            "we",
            "our",
            "you",
            "your",
            "all"
          ]
        }
      },
      "analyzer": {
        "my_analyzer": {
          "tokenizer": "standard",
          "char_filter": [
            "cpp_it"
          ],
          "filter": [
            "lowercase",
            "stop",
            "my_stop"
          ]
        }
      }
    }
  },
  "mappings": {
    "_doc": {
      "properties": {
        "author": {
          "type": "text",
          "fields": {
            "keyword": {
              "type": "keyword",
              "ignore_above": 256
            }
          }
        },
        "category": {
          "type": "keyword"
        },
        "content": {
          "type": "text",
          "fields": {
            "my_analyzer": {
              "type": "text",
              "analyzer": "my_analyzer"
            }
          }
        },
        "publish_date": {
          "type": "date"
        },
        "locales": {
          "type": "keyword"
        },
        "title": {
          "type": "text",
          "fields": {
            "keyword": {
              "type": "keyword",
              "ignore_above": 256
            },
            "my_analyzer": {
              "type": "text",
              "analyzer": "my_analyzer"
            }
          }
        },
        "url": {
          "type": "text",
          "fields": {
            "keyword": {
              "type": "keyword",
              "ignore_above": 256
            }
          }
        }
      }
    }
  }
}

######

POST _reindex
{
  "source": {"index": "blogs"},
  "dest":   {"index": "blogs_analyzed"}
}

######

GET blogs_analyzed/_search
{
  "query": {
    "match": {
      "content.my_analyzer": "c++"
    }
  }
}

GET blogs_analyzed/_search
{
  "query": {
    "match": {
      "title.my_analyzer": "IT"
    }
  }
}

######

{
  "@timestamp": "2017-07-17T00:57:51.548Z",
  "method": "GET",
  "level": "info",
  "user_agent": "Amazon CloudFront",
  "language": {
    "url": "/blog/introducing-machine-learning-for-the-elastic-stack",
    "code": "ko-kr"
  },
  "response_size": 54149,
  "geoip": {
    "country_name": "Singapore",
    "continent_code": "AS",
    "country_code3": "SG",
    "location": {
      "lon": 103.8,
      "lat": 1.3667
    },
    "country_code2": "SG"
  },
  "host": "server1",
  "status_code": 200,
  "originalUrl": "/kr/blog/introducing-machine-learning-for-the-elastic-stack",
  "runtime_ms": 109,
  "http_version": "1.1"
}

######

POST logs_temp/_doc
{
  "@timestamp": "2017-07-17T00:57:51.548Z",
  "method": "GET",
  "level": "info",
  "user_agent": "Amazon CloudFront",
  "language": {
    "url": "/blog/introducing-machine-learning-for-the-elastic-stack",
    "code": "ko-kr"
  },
  "response_size": 54149,
  "geoip": {
    "country_name": "Singapore",
    "continent_code": "AS",
    "country_code3": "SG",
    "location": {
      "lon": 103.8,
      "lat": 1.3667
    },
    "country_code2": "SG"
  },
  "host": "server1",
  "status_code": 200,
  "originalUrl": "/kr/blog/introducing-machine-learning-for-the-elastic-stack",
  "runtime_ms": 109,
  "http_version": "1.1"
}

GET logs_temp/_mapping

########################################################
# Lab 7: Node Types

######

GET _cluster/state

######

GET _cat/nodes?v

######

GET _cat/indices?v

######

cluster.name: my_cluster
discovery.zen.minimum_master_nodes: 2
node.name: node1
network.host:  0.0.0.0
discovery.zen.ping.unicast.hosts: ["server1", "server2", "server3"]
xpack.security.enabled: true

######

cluster.name: my_cluster
node.name: node2
network.host: _site_
discovery.zen.ping.unicast.hosts: ["server1", "server2", "server3"]
discovery.zen.minimum_master_nodes: 2
xpack.security.enabled: true

######

-Xms512m
-Xmx512m

######

ip         heap.percent ram.percent cpu load_1m load_5m load_15m node.role master name
172.18.0.2           31          98  11    0.01    0.15     0.08 mdi       *      node1
172.18.0.3           37          98  11    0.01    0.15     0.08 mdi       -      node2

######

GET _cat/nodes?v

######

cluster.name: my_cluster
node.name: node3
network.host: _site_
discovery.zen.ping.unicast.hosts: ["server1", "server2", "server3"]
discovery.zen.minimum_master_nodes: 2
xpack.security.enabled: true

######

-Xms512m
-Xmx512m

######

PUT test0
{
  "settings": {
    "number_of_replicas": 0
  }
}

######

GET _cat/nodes

######

{
  "statusCode": 500,
  "error": "Internal Server Error",
  "message": "An internal server error occurred"
}

######

node.master: true
node.data: false
node.ingest: false

######

node.master: false
node.data: true
node.ingest: true

######

GET _cat/nodes

######

172.18.0.2 49 96 16 0.24 0.15 0.06 di - node1
172.18.0.4 48 96 11 0.24 0.15 0.06 di - node3
172.18.0.3 32 96 15 0.24 0.15 0.06 m  * node2

########################################################
# Lab 8: Understanding Shards

######

GET _cat/shards?v

######

GET _cat/shards/logs_server2?v

######

PUT test1
{
  "settings": {
    "number_of_shards": 4,
    "number_of_replicas": 2
  }
}

######

GET _cat/shards/test1?v

######

GET _cat/shards/test1?v=shard,prirep

######

GET _cat/shards/test1?v

######

GET _cat/shards/test1?v=shard,prirep

######

PUT test1/_settings
{
  "settings": {
    "number_of_replicas": 0
  }
}

######

GET _cat/shards/test1?v=node,shard

######

GET blogs/_search
{
  "_source": "title",
  "query": {
    "match": {
      "title": "elastic stack"
    }
  }
}

######

GET blogs/_search?search_type=dfs_query_then_fetch
{
  "_source": "title",
  "query": {
    "match": {
      "title": "elastic stack"
    }
  }
}

########################################################
# Lab 9: Troubleshooting Elasticsearch

######

PUT test1/_doc/
{
  "test": "test"
}

######

GET test2/_doc/1

######

GET blogs/_search
{
  "query": {
    "match": {
      "title": "open source software",
      "minimum_should_match": 2
    }
  }
}

######

GET blogs/_search
{
  "query": {
    "match": {
      "title": {
        "query": "open source software",
        "minimum_should_match": 2
      }
    }
  }
}

######

GET blogs/_search
{"query":{"match":{"title":{"query":"open source software,"minimum_should_match":2}}}}

######

PUT test2/_doc/1
{
  "date": "2017-09-10"
}

PUT test3/_doc/1
{
  "date": "September 10, 2017"
}

GET test*/_search
{
  "query": {
    "match": {
      "date": "September"
    }
  }
}

######

GET _cluster/health

######

GET _cluster/health?level=indices

######

GET _cat/indices?v

######

GET _cluster/health/test0?level=shards

######

GET _cat/shards/test0?v

######

GET _cluster/allocation/explain

######

GET _cluster/allocation/explain
{
  "index": "test0",
  "shard": 3,
  "primary": true
}

######

GET _cat/indices

######

PUT test0/_settings
{
  "settings": {
    "number_of_replicas": 1
  }
}

######

PUT test0/_settings
{
  "settings": {
    "number_of_replicas": 0
  }
}

######

PUT _cluster/settings
{
  "transient" : {
    "cluster.routing.allocation.exclude._ip" : "172.18.0.3"
  }
}

######

DELETE test0

######

DELETE test0
DELETE test*

########################################################
# Lab 10: Improving Search Results

######

GET blogs/_search
{
  "query": {
    "match": {
      "content": "open source"
    }
  }
}

######

GET blogs/_search
{
  "query": {
    "match": {
      "title": "open source"
    }
  }
}

######

GET blogs/_search
{
  "query": {
    "multi_match": {
      "query": "open source",
      "fields": [
        "title",
        "content"
      ]
    }
  }
}

######

GET blogs/_search
{
  "query": {
    "multi_match": {
      "query": "open source",
      "fields": [
        "title^2",
        "content"
      ]
    }
  }
}

######

GET blogs/_search
{
  "query": {
    "multi_match": {
      "query": "open source",
      "fields": [
        "title",
        "content"
      ],
      "type": "phrase"
    }
  }
}

######

GET blogs/_search
{
  "_source": "title",
  "query": {
    "match": {
      "title": "oven sauce"
    }
  }
}

######

GET blogs/_search
{
  "_source": "title",
  "query": {
    "match": {
      "title": {
        "query" : "oven sauce",
        "fuzziness": 2
      }
    }
  }
}

######

GET blogs/_search
{
  "_source": "title",
  "query": {
    "match": {
      "title": {
        "query" : "oven sauce",
        "fuzziness": "auto"
      }
    }
  }
}

######

GET blogs/_search
{
  "query": {
    "match_phrase": {
      "content": {
        "query" : "elastic stack"
      }
    }
  },
  "sort": [
    {
      "publish_date": {
        "order": "desc"
      }
    }
  ]
}

######

GET blogs/_search
{
  "query": {
    "match_phrase": {
      "content": {
        "query" : "elastic stack"
      }
    }
  },
  "sort": [
    {
      "author.keyword": {
        "order": "asc"
      }
    },
    {
      "publish_date": {
        "order": "desc"
      }
    }
  ]
}

######

GET blogs/_search
{
  "size": 3,
  "query": {
    "match_phrase": {
      "content": {
        "query" : "elastic stack"
      }
    }
  },
  "sort": [
    {
      "author.keyword": {
        "order": "asc"
      }
    },
    {
      "publish_date": {
        "order": "desc"
      }
    }
  ]
}

######

GET blogs/_search
{
  "from": 9,
  "size": 3,
  "query": {
    "match_phrase": {
      "content": {
        "query" : "elastic stack"
      }
    }
  },
  "sort": [
    {
      "author.keyword": {
        "order": "asc"
      }
    },
    {
      "publish_date": {
        "order": "desc"
      }
    }
  ]
}

######

GET blogs/_search
{
  "from": 9,
  "size": 3,
  "query": {
    "match_phrase": {
      "content": {
        "query" : "elastic stack"
      }
    }
  },
  "sort": [
    {
      "author.keyword": {
        "order": "asc"
      }
    },
    {
      "publish_date": {
        "order": "desc"
      }
    }
  ],
  "highlight": {
    "fields": {
      "title" : {},
      "content" : {}
    },
    "require_field_match": false,
    "pre_tags": ["mark"],
    "post_tags": ["/mark"]
  }
}

######

GET blogs/_search
{
  "from": 9,
  "size": 3,
  "query": {
    "bool": {
      "must": [
        {
          "match_phrase": {
            "content": "elastic stack"
          }
        }
      ],
      "filter": {
        "match": {
          "category.keyword": "Engineering"
        }
      }
    }
  },
  "sort": [
    {
      "author.keyword": {
        "order": "asc"
      }
    },
    {
      "publish_date": {
        "order": "desc"
      }
    }
  ],
  "highlight": {
    "fields": {
      "title" : {},
      "content" : {}
    },
    "require_field_match": false,
    "pre_tags": ["mark"],
    "post_tags": ["/mark"]
  }
}

########################################################
# Lab 11: Aggregating Data

######

GET logs_server*/_search
{
  "size": 0,
  "aggs": {
    "my_url_value_count": {
      "cardinality": {
        "field": "originalUrl.keyword"
      }
    }
  }
}

######

GET logs_server*/_search
{
  "size": 0,
  "query": {
    "match": {
      "originalUrl": "elastic"
    }
  },
  "aggs": {
    "my_url_value_count": {
      "cardinality": {
        "field": "originalUrl.keyword"
      }
    }
  }
}

######

GET logs_server*/_search
{
  "size": 0,
  "aggs": {
    "my_northernmost_request": {
      "max": {
        "field": "geoip.location.lat"
      }
    }
  }
}

######

GET logs_server*/_search
{
  "query": {
    "range": {
      "geoip.location.lat": {
        "gte": 72
      }
    }
  }
}

######

GET logs_server*/_search
{
  "size": 0,
  "aggs": {
    "status_code_cardinality": {
      "cardinality": {
        "field": "status_code"
      }
    }
  }
}

######

GET logs_server*/_search
{
  "size": 0,
  "aggs": {
    "status_code_buckets": {
      "terms": {
        "field": "status_code"
      }
    }
  }
}

######

GET logs_server*/_search
{
  "size": 0,
  "aggs": {
    "status_code_buckets": {
      "terms": {
        "field": "status_code",
        "order": {
          "_key": "asc"
        }
      }
    }
  }
}

######

GET blogs/_search
{
  "query": {
    "bool": {
      "must": {
        "multi_match": {
          "query": "open source",
          "fields": [
            "title^2",
            "content"
          ],
          "type": "phrase"
        }
      }
    }
  },
  "highlight": {
    "fields": {
      "title": {},
      "content": {}
    },
    "require_field_match": false,
    "pre_tags": [
      "mark"
    ],
    "post_tags": [
      "/mark"
    ]
  }
}

######

GET blogs/_search
{
  "query": {
    "bool": {
      "must": {
        "multi_match": {
          "query": "open source",
          "fields": [
            "title^2",
            "content"
          ],
          "type": "phrase"
        }
      }
    }
  },
  "highlight": {
    "fields": {
      "title": {},
      "content": {}
    },
    "require_field_match": false,
    "pre_tags": [
      "mark"
    ],
    "post_tags": [
      "/mark"
    ]
  },
  "aggs": {
    "category_terms": {
      "terms": {
        "field": "category.keyword",
        "size": 10
      }
    }
  }
}

######

GET logs_server*/_search
{
  "size": 0,
  "aggs": {
    "logs_by_week": {
      "date_histogram": {
        "field": "@timestamp",
        "interval": "week"
      }
    }
  }
}

######

GET logs_server*/_search
{
  "size": 0,
  "aggs": {
    "logs_by_week": {
      "date_histogram": {
        "field": "@timestamp",
        "interval": "week"
      },
      "aggs": {
        "status_code_buckets": {
          "terms": {
            "field": "status_code"
          }
        }
      }
    }
  }
}

######

GET logs_server*/_search
{
  "size": 0,
  "aggs": {
    "logs_by_week": {
      "date_histogram": {
        "field": "@timestamp",
        "interval": "week"
      },
      "aggs": {
        "status_code_buckets": {
          "terms": {
            "field": "status_code",
            "show_term_doc_count_error": true
          }
        }
      }
    }
  }
}

######

GET logs_server*/_search
{
  "size": 0,
  "aggs": {
    "top_cities": {
      "terms": {
        "field": "geoip.city_name.keyword",
        "size": 20
      }
    }
  }
}

######

GET logs_server*/_search
{
  "size": 0,
  "aggs": {
    "top_cities": {
      "terms": {
        "field": "geoip.city_name.keyword",
        "size": 20,
        "show_term_doc_count_error": true
      }
    }
  }
}

########################################################
# Lab 12: Best Practices

######

POST _aliases
{
  "actions": [
    {
      "add": {
        "index": "logs_server3",
        "alias": "access_logs_write"
      }
    }
  ]
}

######

POST _aliases
{
  "actions": [
    {
      "add": {
        "index": "logs_server*",
        "alias": "access_logs_read"
      }
    }
  ]
}

######

GET access_logs_read/_search

######

PUT _template/access_logs_template
{
  "index_patterns": "logs_server*",
  "order": 10,
  "settings": {
    "number_of_shards": 5,
    "number_of_replicas": 1
  },
  "mappings": {
    "doc": {
      "properties": {
        "@timestamp": {
          "type": "date"
        },
        "geoip": {
          "properties": {
            "city_name": {
              "type": "text",
              "fields": {
                "keyword": {
                  "type": "keyword",
                  "ignore_above": 256
                }
              }
            },
            "continent_code": {
              "type": "text",
              "fields": {
                "keyword": {
                  "type": "keyword",
                  "ignore_above": 256
                }
              }
            },
            "country_code2": {
              "type": "text",
              "fields": {
                "keyword": {
                  "type": "keyword",
                  "ignore_above": 256
                }
              }
            },
            "country_code3": {
              "type": "text",
              "fields": {
                "keyword": {
                  "type": "keyword",
                  "ignore_above": 256
                }
              }
            },
            "country_name": {
              "type": "text",
              "fields": {
                "keyword": {
                  "type": "keyword",
                  "ignore_above": 256
                }
              }
            },
            "location": {
              "properties": {
                "lat": {
                  "type": "float"
                },
                "lon": {
                  "type": "float"
                }
              }
            },
            "region_name": {
              "type": "text",
              "fields": {
                "keyword": {
                  "type": "keyword",
                  "ignore_above": 256
                }
              }
            }
          }
        },
        "host": {
          "type": "text",
          "fields": {
            "keyword": {
              "type": "keyword",
              "ignore_above": 256
            }
          }
        },
        "http_version": {
          "type": "text",
          "fields": {
            "keyword": {
              "type": "keyword",
              "ignore_above": 256
            }
          }
        },
        "language": {
          "properties": {
            "code": {
              "type": "text",
              "fields": {
                "keyword": {
                  "type": "keyword",
                  "ignore_above": 256
                }
              }
            },
            "url": {
              "type": "text",
              "fields": {
                "keyword": {
                  "type": "keyword",
                  "ignore_above": 256
                }
              }
            }
          }
        },
        "level": {
          "type": "text",
          "fields": {
            "keyword": {
              "type": "keyword",
              "ignore_above": 256
            }
          }
        },
        "method": {
          "type": "text",
          "fields": {
            "keyword": {
              "type": "keyword",
              "ignore_above": 256
            }
          }
        },
        "originalUrl": {
          "type": "text",
          "fields": {
            "keyword": {
              "type": "keyword",
              "ignore_above": 256
            }
          }
        },
        "response_size": {
          "type": "long"
        },
        "runtime_ms": {
          "type": "long"
        },
        "status_code": {
          "type": "long"
        },
        "user_agent": {
          "type": "text",
          "fields": {
            "keyword": {
              "type": "keyword",
              "ignore_above": 256
            }
          }
        }
      }
    }
  }
}

######

PUT logs_server4

GET logs_server4

######

POST _aliases
{
  "actions": [
    {
      "remove": {
        "alias": "access_logs_write",
        "index": "logs_server3"
      }
    },
    {
      "add": {
        "alias": "access_logs_write",
        "index": "logs_server4"
      }
    }
  ]
}

######

{
  "@timestamp": "2018-03-21T05:57:19.722Z",
  "originalUrl": "/blog/logstash-jdbc-input-plugin",
  "host": "server2",
  "response_size": 58754,
  "status_code": 200,
  "method": "GET",
  "runtime_ms": 143,
  "geoip": {
    "country_code2": "IN",
    "country_code3": "IN",
    "continent_code": "AS",
    "location": {
      "lon": 77.5833,
      "lat": 12.9833
    },
    "region_name": "Karnataka",
    "city_name": "Bengaluru",
    "country_name": "India"
  },
  "language": {
    "url": "/blog/logstash-jdbc-input-plugin",
    "code": "en-us"
  },
  "user_agent": "Amazon CloudFront",
  "http_version": "1.1",
  "level": "info"
}

######

PUT access_logs_write/doc/1
{
  "@timestamp": "2018-03-21T05:57:19.722Z",
  "originalUrl": "/blog/logstash-jdbc-input-plugin",
  "host": "server2",
  "response_size": 58754,
  "status_code": 200,
  "method": "GET",
  "runtime_ms": 143,
  "geoip": {
    "country_code2": "IN",
    "country_code3": "IN",
    "continent_code": "AS",
    "location": {
      "lon": 77.5833,
      "lat": 12.9833
    },
    "region_name": "Karnataka",
    "city_name": "Bengaluru",
    "country_name": "India"
  },
  "language": {
    "url": "/blog/logstash-jdbc-input-plugin",
    "code": "en-us"
  },
  "user_agent": "Amazon CloudFront",
  "http_version": "1.1",
  "level": "info"
}

GET logs_server4/doc/1

######

DELETE my_blogs
PUT my_blogs/_doc/1
{
  "id": "1",
  "title": "Better query execution",
  "category": "Engineering",
  "date":"July 15, 2015",
  "author":{
    "first_name": "Adrian",
    "last_name": "Grand",
    "company": "Elastic"
  }
}
PUT my_blogs/_doc/2
{
  "id": "2",
  "title": "The Story of Sense",
  "date":"May 28, 2015",
  "author":{
    "first_name": "Boaz",
    "last_name": "Leskes"
  }
}

######

{
  "title": "Using Elastic Graph",
  "category": "Engineering",
  "date": "May 25, 2016",
  "author": {
    "first_name": "Mark",
    "last_name": "Harwood",
    "company": "Elastic"
  }
}

######

POST my_blogs/_doc/_bulk
{"update": {"_id": 2}}
{"doc": {"category": "Engineering", "author.company":"Elastic"}}
{"index": {"_id": 3}}
{"title":"Using Elastic Graph","category":"Engineering","date":"May 25, 2016","author":{"first_name":"Mark","last_name":"Harwood","company":"Elastic"}}

GET my_blogs/_search

######

GET my_blogs/_doc/_mget
{
  "docs": [
    {"_id":1},
    {"_id":2}
  ]
}

######

GET blogs/_search?scroll=3m
{
  "size": 500,
  "query": {
    "match_all": {}
  },
  "sort": [
    {
      "_doc": {
        "order": "asc"
      }
    }
  ]
}

######

GET _search/scroll
{
  "scroll": "3m",
  "scroll_id": "put_your_scroll_id_here"
}

######

path.repo: /shared_folder/my_repo

######

PUT _snapshot/my_local_repo
{
  "type": "fs",
  "settings": {
    "location": "/shared_folder/my_repo"
  }
}

######

PUT _snapshot/my_local_repo/blogs_snapshot_1
{
  "indices": "blogs",
  "ignore_unavailable": true,
  "include_global_state": true
}

######

GET _snapshot/my_local_repo/_all

######

DELETE blogs

######

POST _snapshot/my_local_repo/blogs_snapshot_1/_restore
{
  "indices": "blogs",
  "ignore_unavailable": true,
  "include_global_state": false
}

